#! /usr/bin/python3

import numpy as np
import pylab as pl

shiftx = 10

all_time = 10000

pfi_arr = np.zeros(all_time, dtype=float)
pfi_arr_raw = np.zeros(all_time, dtype=float)
deriv_arr = np.zeros(all_time, dtype=float)
w_arr = np.zeros(all_time, dtype=float)

delta_pfi = 0.0000000000000001

delta_pfi_raw = delta_pfi

w = 0

delta_pfi_old = 0

for i in range(all_time):

    # Making random walk signal with limitations; this version will get control on it
    z = np.random.rand()
    
    delta_pfi = delta_pfi + 0.001*np.sign(z-0.5)
    
    if delta_pfi > 0.05:
        delta_pfi = 0.05
    
    if delta_pfi < -0.05:
        delta_pfi = -0.05

    # Making random walk signal with limitations; this is just to look what would have happened if there were no control
    
    delta_pfi_raw = delta_pfi_raw + 0.001*np.sign(z-0.5)
    if delta_pfi_raw > 0.05:
        delta_pfi_raw = 0.05

    if delta_pfi_raw < -0.05:
        delta_pfi_raw = -0.05
  
    # Saving values of the signal 

    pfi_arr[i] = delta_pfi

    # learning, learning speed here is 1000

    if i > shiftx:
        w = w + 1000*deriv_arr[i-shiftx]*pfi_arr[i]
        w_arr[i] = w

        # Control rate, its essentially the learned weights w. But was checking with constant, too.
        control = w*1

        pfi_arr_raw[i] = delta_pfi_raw  # Here I am collecting random walk signal in case it was not controlled, not very orderly, should have done higher

        delta_pfi = delta_pfi - control*delta_pfi # adding control here

        # calculating derivative
        if i>1:
            deriv = delta_pfi-delta_pfi_old
            deriv_arr[i] = deriv

        delta_pfi_old = delta_pfi

pl.subplot(311)
pl.plot(pfi_arr, 'b')

pl.subplot(312)
pl.plot(pfi_arr_raw, 'r')

pl.subplot(313)
pl.plot(w_arr, 'g')

pl.show()

import ipdb; ipdb.set_trace()

