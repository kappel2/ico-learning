#!/usr/bin/env python3
"""
This example implements a noise reduction mechanism using ICO [1].

David Kappel
July 2021

References:

[1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
convergence of temporal sequence learning by using input correlations only."
Neural computation 18.6 (2006): 1380-1412.

https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
"""

import numpy as np
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']

import matplotlib.pyplot as pl
import progressbar

from scipy.signal import butter, lfilter, freqz
from core.noise_cancellation_controller import Arena
from core.network import ICONet


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y
    

def to_db(input):
    return 10*np.log10( (np.abs(input + 1e-5 )**2).mean(-1) )


def ico_noise_cancellation(T_fade=-1, T_max=4, T_start=4, input_filter=False,
                           input_noise=0, cutoff=None, plot=True, distortion=False,
                           T_swap_inputs=None):

    # number of filter banks
    N = 3

    # number of batches
    num_batches = 1

    # max simulation time per batch, convert times to num samples
    fs = 22050
    T_max = int(fs*T_max)
    T_fade = int(fs*T_fade)
    T_start = int(fs*T_start)

    # speed regulation for controller
    ctrl_speed = 5

    w_0 = [1.0, 0.6, 0.8]
    w_1 = [0.5, 0.1, 1.0]

    normed_time_to_target = []
    
    mixing_weights = np.tile(w_0,[T_max,1])
    
    if T_fade > 0:
        mixing_weights[T_start:(T_start+T_fade),:] = np.linspace(w_0, w_1, T_fade)
        mixing_weights[(T_start+T_fade):,:] = np.tile(w_1,[T_max-T_start-T_fade,1])

    for batch in range(num_batches):

        w_init = np.zeros(N+1)
        w_init[0] = 1.0
        
        w_hist = np.full([N+1, T_max], np.nan)
        
        arena = Arena(T_max=T_max, visible=plot, input_noise=input_noise)
        
        if distortion:        
            h1 = np.linspace(0,1,fs//2)
            h1 = 2.0*np.hstack([h1,h1[::-1]])
            distortion_data = np.load("data/ambulance.npz")["data"]
            distortion_weights = np.zeros_like(distortion_data)
            distortion_weights[0,int(fs*2.0):int(fs*(2.0+1))] = h1
            distortion_weights[1,int(fs*2.5):int(fs*(2.5+1))] = h1
            distortion_weights[2,int(fs*3.0):int(fs*(3.0+1))] = h1
            arena.ambient_data += distortion_data*distortion_weights
            arena.in_data += distortion_data*distortion_weights
            
            if plot:
                pl.plot( np.arange(0,5*fs)/fs, arena.ambient_data[0,:5*fs] + 10, 'b-' )                                     
                pl.plot( np.arange(0,5*fs)/fs, arena.ambient_data[1,:5*fs] + 5, 'b-' )                                      
                pl.plot( np.arange(0,5*fs)/fs, arena.ambient_data[2,:5*fs] + 0, 'b-' )                                      
                pl.xlabel('time [s]')
                pl.show()
                
        if cutoff is not None:
            for i in range(N):
                arena.ambient_data[i,:] = butter_lowpass_filter(arena.ambient_data[i,:],cutoff,fs)
            
        if input_filter:
            net = ICONet(f=np.hstack([0.01, 0.1/(np.arange(N)+1)]), Q=0.51, w_init=w_init, learning_rate=0.1)
        else:
            net = ICONet(f=np.ones(N+1), Q=None, w_init=w_init, learning_rate=0.2)
        
        for t in progressbar.progressbar(range(1,T_max)):
            
            ## controller through ICO network            
            # run network on inputs
            
            ctrl = net.update(np.append(arena._effective_noise[t-1], arena._noise_sources[:,t-1]))
            
            arena.weights = mixing_weights[t]

            w_hist[:,t] = net.W

            arena.move(w_hist[1:,t])
            
            arena.update(batch, t, show={"weights":0.5*w_hist})
            
            if T_swap_inputs and t >= T_swap_inputs*fs:
                net.learning_rate = 0
                arena.in_data = arena.ambient_data
                #net.W[1:] /= (net.W[1:]/arena.weights).mean()

        arena.close()
        
    snr_times = np.arange(0,T_max,fs//5)/fs
    snrs =  to_db((arena._noise_sources * mixing_weights.T).sum(0).reshape([-1,fs//5])) - \
            to_db(arena._effective_noise.reshape([-1,fs//5]))
    
    if plot:
        import ipdb; ipdb.set_trace()
        
        pl.plot( snr_times, snrs ) 
        pl.xlabel("time [s]")
        pl.ylabel("noise reduction [dB]")
        
        pl.show()
        
        import ipdb; ipdb.set_trace()
        
        subs = 100
        
        #pl.subplot(411)
        #pl.plot( np.arange(0,T_max,subs)/fs, arena._noise_sources.T[:,::subs] )
        #pl.legend(["x1","x2","x3"])
        #pl.title("noise sources")
        
        pl.subplot(411)
        pl.plot( np.arange(0,T_max,subs)/fs, (arena._noise_sources * mixing_weights.T).sum(0)[::subs] )
        pl.title("mixed noise sources")
        
        pl.subplot(412)
        pl.plot( np.arange(0,T_max,subs)/fs, arena._effective_noise[::subs] )
        pl.title("effective noise")    
        
        pl.subplot(413)
        pl.plot( np.arange(0,T_max,subs)/fs,  w_hist[1:,:].T[::subs,:] )
        pl.plot( np.arange(0,T_max,subs)/fs,  mixing_weights[::subs,:], '--' )
        pl.legend(["w1","w2","w3", "w1_opt", "w2_opt", "w3_opt"])
        pl.title("synaptic weights")
        
        pl.subplot(414)
        pl.plot( snr_times, snrs )
        pl.xlabel("time [s]")
        pl.ylabel("noise reduction [dB]")
        pl.title("noise reduction")
        pl.show()
    
        import ipdb; ipdb.set_trace()
        
        # pl.specgram((arena.ambient_data.T * arena.weights).sum(1), Fs=4400, mode='magnitude', scale='dB', cmap='jet', vmin=-150, vmax=0)
        # pl.specgram((arena.ambient_data.T * arena.weights).sum(1) - (arena.input_data.T * arena.weights).sum(1), Fs=4400, mode='magnitude', scale='dB', cmap='jet', vmin=-150, vmax=0)
        
        
    noise_snr = to_db(arena._noise_sources) - to_db(arena._inputs - arena._noise_sources)
    
    return {"snr_times":snr_times, "snr_values":snrs, "noise_snr":noise_snr}
        

if __name__=="__main__":

    #results = ico_noise_cancellation(T_max=10, T_start=3, T_fade=5, plot=True)

    # use distortion data (ambulance)
    # results = ico_noise_cancellation(T_max=5, plot=True, distortion=True)
    
    # use low pass filter
    results = ico_noise_cancellation(T_max=20, plot=True, cutoff=9000, T_swap_inputs=18)
    
    # no input filter
    #results = ico_noise_cancellation(T_max=5, plot=True)

    import ipdb; ipdb.set_trace()

