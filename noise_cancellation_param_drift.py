#!/usr/bin/env python3

import numpy as np
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']

import matplotlib.pyplot as pl

from ico_noise_cancellation import ico_noise_cancellation


if __name__=="__main__":

    T_fade_all = np.arange(0.5,7.0,1.0)
    
    num_runs = 5
    
    snr_times_all_runs = []
    snrs_all_runs = []
    
    for run in range(num_runs):
        
        snr_times_all = []
        snrs_all = []

        for i in range(len(T_fade_all)):
            print("run #"+str(i))
            results = ico_noise_cancellation(T_fade=T_fade_all[i], T_max=10, T_start=3, plot=False)
            
            snr_times_all += [results["snr_times"]]
            snrs_all += [results["snr_values"]]
            
            print("mean noise reduction: "+str(snrs_all[-1][-10:].mean()))

        snr_times_all_runs += [snr_times_all]
        snrs_all_runs += [snrs_all]

    import ipdb; ipdb.set_trace()

    fig = pl.figure(figsize=(3.5,2.8))

    pl.errorbar( T_fade_all, np.array( snrs_all )[:,15:20].mean(1), \
                 yerr=np.array( snrs_all )[:,15:20].std(1) )
                 
    pl.xlabel("parameter drift speed")
    pl.ylabel("noise reduction [dB]")
    
    pl.savefig("results/noise_cencellation_param_drift.eps")
    
    import ipdb; ipdb.set_trace()

