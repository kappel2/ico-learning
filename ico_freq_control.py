#!/usr/bin/env python3
"""
This example implements the closed loop robot environment from Figure 3 in [1].

References:

[1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
convergence of temporal sequence learning by using input correlations only."
Neural computation 18.6 (2006): 1380-1412.

https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
"""

import numpy as np
import pylab as pl

from core.freq_controller import Arena
from core.network import ICONet

# number of filter banks
N = 1

# number of batches
num_batches = 10

# max simulation time per batch
T_max = 20000

# speed regulation for controller
ctrl_speed = 5

def main():

    normed_time_to_target = []

    for batch in range(num_batches):

        w_init = np.zeros(N+1)
        w_init[0] = -1.0
        w_init[1] = 0.5
        
        w_hist = np.full([N+1, T_max], np.nan)
        
        arena = Arena(visible=True)   
        net = ICONet(f=np.hstack([0.01, 0.1/(np.arange(N)+1)]), Q=0.51, w_init=w_init)
            
        for t in range(T_max):
            
            ## controller through ICO network            
            # run network on inputs
            
            ctrl = net.update(np.array([arena._traj_0[-1], arena._traj_1[-1]])) 

            w_hist[:,t] = net.W

            arena.move(0.1*ctrl) # net.W[1])
            
            arena.update(batch, t, show={"weights":w_hist})

        arena.close()
    
        normed_time_to_target += [ ( np.diff(arena.target_time_hist) / np.sqrt(np.sum(np.diff(np.vstack([[0,0]]+arena.target_pos_hist), axis=0)**2,axis=1))[:-1] ) ]
        
        print("batch: {}, num trials: {}".format(batch, len(normed_time_to_target[-1])))

    import ipdb; ipdb.set_trace()
    
    y_val = np.stack( [a[:15] for a in normed_time_to_target[:5]] ).mean(0)[1:11]/500                 
    y_err = np.stack( [a[:15] for a in normed_time_to_target[:5]] ).std(0)[1:11]/np.sqrt(5)/500
    pl.bar( np.arange(1,11), y_val, color=[.4,.4,1.0], label='mean' )                                 
    pl.bar( np.arange(1,11), y_err, bottom=y_val, color=[.5,.5,1.0], label='s.e.m.' )                 
    pl.plot( [0.5,10.5], [1,1], 'k-', label='optimal' )
    pl.xlabel('trial')
    pl.ylabel('normalized time to target')
    pl.legend()
    pl.show()
    

if __name__=="__main__":
    main()
    

