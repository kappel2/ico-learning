A simple implementation the ICO model [1] for closed and open loop learning.


[1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster convergence of temporal sequence learning by using input correlations only." Neural computation 18.6 (2006): 1380-1412.

https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf


## Get started

To get started run

`python3 ico_learning_conditioning.py`

`python3 ico_learning_robot.py`

`python3 ico_noise_cancellation.py`

## Required packages

`numpy`
`matplotlib`

