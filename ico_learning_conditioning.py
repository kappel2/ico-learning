#!/usr/bin/env python3
"""
This example implements the open loop experiment from Figure 2 in [1].

References:

[1] Temporal hebbian learning in rate-coded neural networks: A theoretical approach
towards classical conditioning. B Porr, F Wörgötter - International Conference
on Artificial Neural Networks, 2001

https://dl.acm.org/doi/abs/10.5555/646258.686209
"""

import numpy as np
import pylab as pl

from core.network import ICONet
from core.spike_protocol import SpikeProtocol

# number of filter banks
N = 1

# number of batches
num_batches = 5

# max simulation time per batch
T_max = 200


def main():
    w_init = np.zeros(N+1)
    w_init[0] = 1.0
    
    w_hist = np.full([N+1, T_max], np.nan)
    
    x = np.zeros([N+1,T_max])
    x[0,30] = 1
    x[1,10] = 1

    net = ICONet(f=np.hstack([0.01, 0.01/(np.arange(N)+1)]), Q=0.51, w_init=w_init, learning_rate=0.01)
    
    env = SpikeProtocol(x, net)

    env.run(num_batches)
    env.close()
    

if __name__=="__main__":
    main()

