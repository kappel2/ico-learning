import numpy as np
import pylab as pl
import progressbar

T = 2000

t = np.arange(T)

w_range = np.arange(0.001,0.05,0.001)

learning_rate = 0.01


# analytical solution
def weigth_dynamics(t, w1, w0, a1, a0):

    u0 = np.sin(w0*t + a0)
    u1 = np.sin(w1*t + a1)
    w = np.zeros_like(u0)

    for t in range(T-1):
        w[t+1] = w[t] + learning_rate*u1[t+1]*(u0[t+1]-u0[t])
    
    return w


def weigth_dynamics_analytical(t, w1, w0, a1, a0):
    if w1 == w0:
        y = 1/4 * ( 2*t*w1*np.sin(a1-a0) + np.cos(a0 + a1) - np.cos(a0 + a1 + 2*t*w1) )
    else:
        y = 1/2 * w0 * ( np.cos(a1-a0)/(w1 - w0) + \
                    np.cos(a0+a1)/(w0 + w1) - \
                    np.cos(a1 - a0 + t*(w1 - w0))/(w1 - w0) - \
                    np.cos(a0 + a1 + t*(w0 - w1))/(w0 + w1) )

    return learning_rate*y


# test approach w1 - w0 -> 0


w1 = 50

w0_all = w1 + np.arange(-0.01,0.01,0.00001)
s_all = np.arange(0,T,T//100)
freq_response_analytical = np.zeros([w0_all.shape[0],s_all.shape[0]])

phase = 0

###

phi1 = 50
phi0 = phi1
theta1 = 0
theta0 = 0

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(331)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.0001
theta1 = 0
theta0 = 0

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(332)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1-0.0001
theta1 = 0
theta0 = 0

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(333)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1-0.000001
theta1 = 0
theta0 = 0

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(334)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.0001
theta1 = 0.8
theta0 = 1.5

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(335)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.1
theta1 = 0.8
theta0 = 1.5

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(336)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.01
theta1 = 0.8
theta0 = 1.5

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(337)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.xlabel('time')
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.001
theta1 = 0.8
theta0 = 1.5

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(338)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.xlabel('time')
pl.ylabel(r'$w(t)$')

###

phi1 = 50
phi0 = phi1+0.001
theta1 = 0.8
theta0 = -0.7

y = weigth_dynamics_analytical(t, phi1/1000, phi0/1000, theta1, theta0)

pl.subplot(339)

pl.plot(y)
#pl.ylim([-800,800])
pl.title(r'$\phi_0=$'+str(phi0)+r'$, \phi_1=$'+str(phi1)+r'$, \theta_0=$'+str(theta0)+r'$, \theta_1=$'+str(theta0))
pl.xlabel('time')
pl.ylabel(r'$w(t)$')

pl.show()

import ipdb; ipdb.set_trace()

pl.subplot(211)

for i in progressbar.progressbar(range(w0_all.shape[0])):
    w0 = w0_all[i]
    y = weigth_dynamics_analytical(t, w0, w1, phase, phase)
    for j in range(s_all.shape[0]):
        freq_response_analytical[i,j] = y[:s_all[j]].mean()
    
    if i%10 == 0:
        c = i/w0_all.shape[0]
        pl.plot(y,color=[0.5,c,1.0-c])
        pl.plot([T+500,T+800],[c*1000-500]*2,color=[0.5,c,1.0-c])

pl.title('detailed analysis of weight dynamics          ')
pl.plot([T+500,T+800,T+800,T+500,T+500], [-500,-500,500,500,-500], 'k-')

pl.text(6000, 0, r'  $\phi_1 - \phi_0$', horizontalalignment='left', verticalalignment='center', rotation='vertical')
pl.text(6000, -500, '-0.01', horizontalalignment='left', verticalalignment='center')
pl.text(6000, 500, '+0.01', horizontalalignment='left', verticalalignment='center')
pl.box(False)
        
pl.xlabel('time [t]')
pl.ylabel('w(t)')

pl.subplot(212)

X = w0_all[-1]*1.00005
U = w0_all[-1]*1.00008

for j in range(s_all.shape[0]):
    c = j/s_all.shape[0]
    pl.plot(w0_all, freq_response_analytical[:,j], color=[c,1.-c,1.-c]);
    pl.plot([X,U],[c*400-200]*2,color=[c,1.-c,1.-c])


pl.title('detailed analysis of weight dynamics          ')
pl.plot([X,U,U,X,X], [-200,-200,200,200,-200], 'k-')

pl.text(U+0.001, 0, 'learning time', horizontalalignment='left', verticalalignment='center', rotation='vertical')
pl.text(U+0.001, -200, '0', horizontalalignment='left', verticalalignment='center')
pl.text(U+0.001, 200, str(T), horizontalalignment='left', verticalalignment='center')
pl.box(False)

pl.xlabel(r'$\phi_0$')
pl.ylabel('avg. weight change')

pl.show()

import ipdb; ipdb.set_trace()

# analyze frequency dependence

phase = 0

w_range = np.arange(0.005,0.1,0.0005)

w1_all, w0_all = np.meshgrid(w_range,w_range)

freq_response = np.zeros_like(w1_all)

freq_response_analytical = np.zeros_like(w1_all)

for i in progressbar.progressbar(range(w1_all.shape[0])):
    for j in range(w1_all.shape[1]):
        y = weigth_dynamics_analytical(t, w1_all[i,j], w0_all[i,j], phase, phase)
        freq_response_analytical[i,j] = y[-T//2:].mean()
        y = weigth_dynamics(t, w1_all[i,j], w0_all[i,j], phase, phase)
        freq_response[i,j] = y[-T//2:].mean()

pl.subplot(221)

pl.imshow( freq_response, extent=[w_range[0],w_range[-1],w_range[0],w_range[-1]], aspect='auto' )
pl.xlabel(r'$\phi_1$')
pl.ylabel(r'$\phi_0$')
cbar = pl.colorbar()
#cbar.set_label(r'$w$', rotation=90)

pl.subplot(222)

pl.plot( w_range[:-1] - w_range[-1]/2, np.diag(freq_response[-1:0:-1,:]) )

pl.xlabel(r'$\phi_1 - \phi_0$')
pl.ylabel(r'$w$')

pl.subplot(223)

pl.imshow( freq_response_analytical, extent=[w_range[0],w_range[-1],w_range[0],w_range[-1]], aspect='auto' )
pl.xlabel(r'$\phi_1$')
pl.ylabel(r'$\phi_0$')
cbar = pl.colorbar()
#cbar.set_label(r'$w$', rotation=90)

pl.subplot(224)

pl.plot( w_range[:-1] - w_range[-1]/2, np.diag(freq_response_analytical[-1:0:-1,:]) )

pl.xlabel(r'$\phi_1 - \phi_0$')
pl.ylabel(r'$w$')

import ipdb; ipdb.set_trace()

pl.show()


# analyze phase dependence

p_range = np.arange(-5,5,0.05)

w_range = np.arange(0.005,0.1,0.0005)

w0 = w_range[w_range.shape[0]//2]

w1_all, p_all = np.meshgrid(w_range, p_range)

phase_response = np.zeros_like(w1_all)

phase_response_analytical = np.zeros_like(w1_all)

for i in progressbar.progressbar(range(w1_all.shape[0])):
    for j in range(w1_all.shape[1]):
        y = weigth_dynamics_analytical(t, w1_all[i,j], w0, p_all[i,j], 0)
        phase_response_analytical[i,j] = y[-T//2:].mean()
        y = weigth_dynamics(t, w1_all[i,j], w0, p_all[i,j], 0)
        phase_response[i,j] = y[-T//2:].mean()

pl.subplot(121)

pl.imshow( phase_response, extent=[w_range[0],w_range[-1],p_range[0],p_range[-1]], aspect='auto' )
pl.xlabel(r'$\phi_1$')
pl.ylabel(r'$\theta_0$')
cbar = pl.colorbar()
cbar.set_label(r'$w$', rotation=90)

pl.subplot(122)

pl.imshow( phase_response_analytical, extent=[w_range[0],w_range[-1],p_range[0],p_range[-1]], aspect='auto' )
pl.xlabel(r'$\phi_1$')
pl.ylabel(r'$\theta_0$')
cbar = pl.colorbar()
cbar.set_label(r'$w$', rotation=90)

import ipdb; ipdb.set_trace()

pl.show()

# analyze phase dependence

p_range = np.arange(-5,5,0.05)

w_range = np.arange(0.005,0.1,0.0005)

w0 = w_range[w_range.shape[0]//2]

p1_all, p0_all = np.meshgrid(p_range, p_range)

phase_response = np.zeros_like(w1_all)

phase_response_analytical = np.zeros_like(w1_all)

for i in progressbar.progressbar(range(w1_all.shape[0])):
    for j in range(w1_all.shape[1]):
        y = weigth_dynamics_analytical(t, w0, w0, p1_all[i,j], p0_all[i,j])
        phase_response_analytical[i,j] = y[-T//2:].mean()
        y = weigth_dynamics(t, w0, w0, p1_all[i,j], p0_all[i,j])
        phase_response[i,j] = y[-T//2:].mean()
        

pl.subplot(121)

pl.imshow( phase_response, extent=[w_range[0],w_range[-1],p_range[0],p_range[-1]], aspect='auto' )
pl.xlabel(r'$\theta_1$')
pl.ylabel(r'$\theta_0$')
cbar = pl.colorbar()
cbar.set_label(r'$w$', rotation=90)

pl.subplot(122)

pl.imshow( phase_response_analytical, extent=[w_range[0],w_range[-1],p_range[0],p_range[-1]], aspect='auto' )
pl.xlabel(r'$\theta_1$')
pl.ylabel(r'$\theta_0$')
cbar = pl.colorbar()
cbar.set_label(r'$w$', rotation=90)

import ipdb; ipdb.set_trace()

pl.show()



offs = T/2

u0 = (t>offs)*(np.exp(-(t-offs)/50) - np.exp(-(t-offs)/5))

u1 = (t>offs)*(np.exp(-(t-offs)/50) - np.exp(-(t-offs)/5))

w = np.zeros(T)


for t in range(T-1):
    w[t+1] = w[t] + learning_rate*u1[t+1]*(u0[t+1]-u0[t])


fU0 = np.fft.fft(u0)

fU1 = np.fft.fft(u1)

fW = np.convolve(np.arange(fU0.shape[0])*fU0, fU1, mode="same")

w_spectral = learning_rate*np.fft.ifft(fW/(np.arange(fU0.shape[0])+1e-5))


pl.subplot(411)
pl.plot( u0[::2] )
pl.plot( u1[::2] )
pl.ylabel("u0(t) / u1(t)")
pl.xlabel("time")

pl.subplot(412)
pl.plot( fU0 )
pl.plot( fU1 )
pl.ylabel("U0(x) / U1(x)")
pl.xlabel("frequency")

pl.subplot(413)
pl.plot( fW )
pl.ylabel("W(x)")
pl.xlabel("frequency")

pl.subplot(414)

pl.plot( -0.0008*(np.real(w_spectral)[::2] - np.real(w_spectral)[0]), label="reconstructed from iFT" )
pl.plot( w[::2], label="original ICO" )
pl.legend()

pl.ylabel("w(t)")
pl.xlabel("time")


pl.show()


