#! /usr/bin/python3

import numpy as np
import sounddevice as sd
import librosa

# sounds sourced from: https://freesound.org/ (creative commons license)

#y1, sr = librosa.load('data/Freesound - Ambience, London Street, A.wav by InspectorJ.mp3')
#y2, sr = librosa.load('data/Freesound - citystreet3.wav by sagetyrtle.mp3')
#y3, sr = librosa.load('data/Freesound - london.mp3 by Walter_Odington.mp3')

y3, sr = librosa.load('data/Freesound - Ambulance siren by stereobrother.mp3')

import ipdb; ipdb.set_trace()

fs = 44100
#data = np.random.uniform(0, 1, fs)
data = np.sin(np.arange(fs)/50)
sd.play(data, fs)



import ipdb; ipdb.set_trace()

