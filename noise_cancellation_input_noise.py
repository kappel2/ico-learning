#!/usr/bin/env python3

import numpy as np
import pylab as pl

from ico_noise_cancellation import ico_noise_cancellation


if __name__=="__main__":

    input_noise_all = np.array([0.0,0.0001,0.0002,0.001,0.002,0.004,0.006,0.008,0.01])
    
    snr_times_all = []
    noise_snr_all = []
    snrs_all = []

    for i in range(len(input_noise_all)):
        print("run #"+str(i))
        results = ico_noise_cancellation(input_noise=input_noise_all[i], plot=False)
        
        noise_snr_all += [results["noise_snr"]]
        snr_times_all += [results["snr_times"]]
        snrs_all += [results["snr_values"]]
        
        print("mean noise reduction: "+str(snrs_all[-1][-10:].mean()))
    
    import ipdb; ipdb.set_trace()

    pl.errorbar( np.array(noise_snr_all).mean(1), \
                 np.array( snrs_all )[:,-10:].mean(1), \
                 yerr=np.array( snrs_all )[:,-10:].std(1) )

    pl.plot( np.array(noise_snr_all).mean(1), np.array( snrs_all ).mean(1), '.-' )
    pl.xlabel("SNR snensor [dB]")
    pl.ylabel("noise reduction [dB]")

    pl.show()
    
