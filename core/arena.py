import numpy as np

from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']

import matplotlib.pyplot as pl



from .environment import Environment

class Arena(Environment):
    """
    Arena implements the closed loop robot environment from Figure 3 in [1].
    
    References:
    
    [1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
    convergence of temporal sequence learning by using input correlations only."
    Neural computation 18.6 (2006): 1380-1412.
    
    https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
    """
    def __init__(self, visible=True, draw_trajectory=False):
        super().__init__(visible, plot_interval=10)
        self.num_sensors = 2
        self.agent_size = 0.12
        self.sensor_dist = 0.08
        self.touch_dist = 0.12
        self.target_size = 0.15
        self.sensor_range = 2.0
        self.max_target_dist = 0.15
        self.min_target_time = 10
        self.speed = 0.001
        self.t = 0
        self.batch = -1
        self.draw_trajectory = draw_trajectory
        self.reset()
        

    def reset(self):
        self.agent_pos = np.zeros(3)
        self.trajectories = []
        self.target_pos_hist = []
        self.target_time_hist = []
        self.sensor_pos = np.zeros([self.num_sensors,2])
        self.sensor_state = np.zeros(self.num_sensors)
        self.touch_pos = np.zeros([self.num_sensors,2])
        self.touch_state = np.zeros(self.num_sensors)
        self.reset_target()


    def reset_target(self):
        self.target_pos = np.random.rand(2)-0.5
        self.target_time = 0
        self.trajectories += [self.agent_pos]
        self.target_pos_hist += [self.target_pos]
        self.target_time_hist += [self.t]


    def draw_content(self):

        cir_target = pl.Circle(self.target_pos, self.target_size, color=[1,.7,.7])
        self.axes.add_patch(cir_target)
        
        cir_agent = pl.Circle(self.agent_pos[:2], self.agent_size, color='k', fill=False)
        self.axes.add_patch(cir_agent)
        
        if self.draw_trajectory:
            pl.plot(self.trajectories[-1][:,0], self.trajectories[-1][:,1], 'b-')
        
        for i in range(self.num_sensors):
            cir_sensor = pl.Circle(self.sensor_pos[i,:], 0.02, color=[0,self.sensor_state[i],0])
            self.axes.add_patch(cir_sensor)

            cir_sensor = pl.Circle(self.touch_pos[i,:], 0.02, color=[0,0,self.touch_state[i]])
            self.axes.add_patch(cir_sensor)

        pl.axis('off')
        
        self.axes.set_xlim([-1, 1])
        self.axes.set_ylim([-1, 1])
            

    def update_state(self, batch, t):
        self.batch = batch
        self.t = t

        # check if agent reached target
        if np.linalg.norm(self.agent_pos[:2] - self.target_pos[:2]) < self.max_target_dist:
            self.target_time += 1
        
        if self.target_time > self.min_target_time:
            self.reset_target() 
            
        # check if agent exceeded arena boundaries
        new_pos = np.clip(self.agent_pos[:2], -1, 1)
        
        if np.any(new_pos != self.agent_pos[:2]):
            self.agent_pos[2] = (self.agent_pos[2] + np.pi)%(2*np.pi)
            self.agent_pos[:2] = new_pos
    
        # update sensor states
        for i in range(self.num_sensors):
            a = self.agent_pos[2] + (i/(self.num_sensors-1) - 0.5)*np.pi/2
            s_dir = np.array([np.sin(a), np.cos(a)])
            pos = self.agent_pos[:2] + self.sensor_dist*s_dir
            dist = np.linalg.norm(pos - self.target_pos[:2])
            self.sensor_pos[i,:] = pos
            self.sensor_state[i] = np.maximum(0.0, 1.0 - dist/self.sensor_range)
            
            pos = self.agent_pos[:2] + self.touch_dist*s_dir
            dist = np.linalg.norm(pos - self.target_pos[:2])
            self.touch_pos[i,:] = pos
            self.touch_state[i] = dist <= self.target_size

        self.trajectories[-1] = np.vstack([self.trajectories[-1], self.agent_pos])


    def move(self, cmd):
        cmd = np.array(cmd)
        m_dir = np.array([np.sin(self.agent_pos[2]), np.cos(self.agent_pos[2])])
        self.agent_pos[:2] = self.agent_pos[:2] + self.speed*(cmd[0] + cmd[1])*m_dir
        self.agent_pos[2] = (self.agent_pos[2] + np.pi*self.speed*(cmd[0] - cmd[1]))


