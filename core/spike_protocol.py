import numpy as np
import pylab as pl

from .environment import Environment

class SpikeProtocol(Environment):
    """
    SpikeProtocol implements the open loop environment from Figure 2 in [1].
    
    References:
    
    [1] Temporal hebbian learning in rate-coded neural networks: A theoretical approach
    towards classical conditioning. B Porr, F Wörgötter - International Conference
    on Artificial Neural Networks, 2001
    
    https://dl.acm.org/doi/abs/10.5555/646258.686209
    """
    def __init__(self, activity, net, visible=True):
        super().__init__(visible, plot_interval=10)
        self.input_activity = activity
        self.net = net
        self.reset()
        

    def reset(self):
        self.batch = 0
        self.t = 0
        self.w_hist = []
        self.u_hist = []
        self.v_hist = []
        

    def draw_content(self):
        w_line, = pl.plot(self.w_hist[-1][1,:], 'b-')
        u0_line, = pl.plot(self.u_hist[-1][0,:], 'k-')
        u1_line, = pl.plot(self.u_hist[-1][1,:], 'k--')
        v_line, = pl.plot(self.v_hist[-1], 'r-')
        
        pl.legend([w_line,u0_line,u1_line,v_line], ["synaptic weight w1","input u0","input u1","output v"])
        
        pl.xlabel('time step')
        
        self.axes.set_xlim([0, self.input_activity.shape[1]])
        self.axes.set_ylim([0, 20])


    def update_state(self, batch, t):
        
        # run network on inputs
        output = self.net.update(self.input_activity[:,t])

        self.w_hist[-1][:,t] = self.net.W
        self.u_hist[-1][:,t] = self.net.u
        self.v_hist[-1][t] = output


    def move(self, cmd):
        cmd = np.array(cmd)
        m_dir = np.array([np.sin(self.agent_pos[2]), np.cos(self.agent_pos[2])])
        self.agent_pos[:2] = self.agent_pos[:2] + self.speed*(cmd[0] + cmd[1])*m_dir
        self.agent_pos[2] = (self.agent_pos[2] + np.pi*self.speed*(cmd[0] - cmd[1]))


    def run(self, num_batches):
        T_max = self.input_activity.shape[1]
        for batch in range(num_batches):
            self.w_hist += [np.full(self.input_activity.shape, np.nan)]
            self.u_hist += [np.full(self.input_activity.shape, np.nan)]
            self.v_hist += [np.full(self.input_activity.shape[1], np.nan)]
            for t in range(T_max):
                self.update(batch, t)


