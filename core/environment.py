import numpy as np
import pylab as pl


class Environment(object):
    """
    Base class to all environments. Handles common display behavior.
    """
    def __init__(self, visible=True, plot_interval=1):
        self.fig = None
        self.axis = None
        self.live_plot = False
        self.visible = visible
        self.batch = 0
        self.t = 0
        self.plot_interval = plot_interval


    def __del__(self):
        self.close()


    def close(self):
        self.exit_live_plot()
        if self.fig:
            pl.close()
            self.exit_live_plot()
            self.fig = None
            self.axes = None
            

    def new_window(self, **kwargs):
        self.exit_live_plot()
        self.fig = pl.figure(**kwargs)


    def enter_live_plot(self):
        if not self.live_plot:
            self.new_window(figsize=(10.,10.))
            self.axes = self.fig.subplots(1,1)
            self.live_plot = True
            

    def exit_live_plot(self):
        if self.live_plot:
            self.fig = None
            self.axes = None
            self.live_plot = False
            pl.show()


    def draw(self, show):
        if self.visible and self.t%self.plot_interval==0: 
            self.enter_live_plot()
            
            self.axes.clear()

            self.draw_content()
            
            if "weights" in show:
                weights = show["weights"]
                for i in range(weights.shape[0]):
                    self.axes.plot( np.linspace(0.3,0.9,weights.shape[1]), weights[i,:] - 0.8 )
                                    
            self.axes.set_title("batch={}, t={}".format(self.batch, self.t))
            
            pl.pause(0.05)
            

    def update_state(self, batch, t):
        raise NotImplemented()
        

    def draw_content(self):
        raise NotImplemented()


    def move(self, cmd):
        cmd = np.array(cmd)
        m_dir = np.array([np.sin(self.agent_pos[2]), np.cos(self.agent_pos[2])])
        self.agent_pos[:2] = self.agent_pos[:2] + self.speed*(cmd[0] + cmd[1])*m_dir
        self.agent_pos[2] = (self.agent_pos[2] + np.pi*self.speed*(cmd[0] - cmd[1]))


    def update(self, batch, t, show={}):
        self.batch = batch
        self.t = t
        self.update_state(batch, t)
        self.draw(show)
        

