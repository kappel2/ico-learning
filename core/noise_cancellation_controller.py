import numpy as np
import pylab as pl

from .environment import Environment


class Arena(Environment):
    """
    This implements random continuous environment.
    """
    def __init__(self, visible=True, T_max=10000, input_noise=0):
        super().__init__(visible, plot_interval=10000)
        self.all_time = T_max
        self.delta_pfi = 0.0000000000000001
        self.N = 3
        self.delay = np.array([1, 1, 1])
        self.weights = np.array([1, 0.5, 0.8])
        self.from_file = True
        self.input_noise = input_noise
        self.load_data()
        self.reset()
        

    def load_data(self):
        if self.from_file:
            print('loading source files...')
            self.in_data = np.load("data/ambient_street_noise.npz")["data"]
            self.ambient_data = self.in_data.copy()
            #import ipdb; ipdb.set_trace()
            #for i in range(
            

    def reset(self):
        self._noise_sources = np.zeros([self.N, self.all_time], dtype=float)
        self._inputs = np.zeros([self.N, self.all_time], dtype=float)
        self._effective_noise = np.zeros(self.all_time, dtype=float)
        self._deriv_arr = np.zeros([self.N, self.all_time], dtype=float)
        self._delta_pfi_raw = self.delta_pfi
        self._delta_pfi_old = 0
        self.control = 0
        
        
    def draw_content(self):

        zoom = 15

        pl.plot(np.linspace(0,1,self._effective_noise.shape[0]), zoom*self._effective_noise)
        pl.plot(np.linspace(0,1,self._noise_sources.shape[1]), zoom*self._noise_sources[0,:] + 2)

        pl.axis('off')
        
        self.axes.set_xlim([0, 1])
        self.axes.set_ylim([-1, 3])
            

    def update_state(self, batch, t):
    
        # Making random walk signal with limitations; this version will get control on it
        
        t_old = (t - 1 + self.all_time)%self.all_time
        t_delay = (t - self.delay + self.all_time)%self.all_time
        t_cur = t % self.all_time
        
        if self.from_file:
            self._noise_sources[:,t_cur] = self.ambient_data[:,t_cur]
            self._inputs[:,t_cur] = self.in_data[:,t_cur]
        else:
            z = np.random.rand(self.N)
            self._noise_sources[:,t_cur] = (self._noise_sources[:,t_old] + 0.001*np.sign(z-0.5)).clip(-0.05,0.05)
            self._inputs[:,t_cur] = self._noise_sources[:,t_cur]
            
        if self.input_noise > 0:
            self._inputs[:,t_cur] += np.random.randn(self.N)*self.input_noise

        self._effective_noise[t_cur] = (self.weights * self._noise_sources[np.arange(self.N),t_delay]).sum() - \
                                       (self.control * self._inputs[np.arange(self.N),t_delay]).sum()
        

    def move(self, control):
        self.control = control


