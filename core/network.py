import numpy as np
import pylab as pl

from .filter import FilterBank

class ICONet(object):
    def __init__(self, f, Q, w_init, learning_rate=1e-4):
        """
        ICONet object implements the model described in [1]
        
        References:
        
        [1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
        convergence of temporal sequence learning by using input correlations only."
        Neural computation 18.6 (2006): 1380-1412.
        
        https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
        """
        self.filters = FilterBank(f,Q)
        self.W = w_init.copy()
        self.learning_rate = learning_rate

        
    def update(self, inputs):
        """
        Main update method to be called inside main loop on an input vector.        
        """
        # Implements Eq.(2) of [1]
        u, du = self.filters.update(inputs)
        
        # Implements Eq.(3) of [1]
        output = np.dot(u,self.W)
        
        # Implements Eq.(5) of [1]
        self.W[1:] += self.learning_rate * u[1:] * du[0]
        
        #dw = u[1:] * du[0]
        #self.W[1:] += self.learning_rate * dw * (self.W[1:]*(dw<0) + (dw>=0)) # - 0.001*self.W[1:]
        self.W = self.W.clip(0)
        
        self.u = u

        return output
        

