import numpy as np
import pylab as pl

from .environment import Environment

class Arena(Environment):
    """
    This implements random continuous environment.
    """
    def __init__(self, visible=True):
        super().__init__(visible, plot_interval=50)
        self.all_time = 10000
        self.delta_pfi = 0.0000000000000001
        self.reset()
        

    def reset(self):
        self._pfi_arr = np.zeros(self.all_time, dtype=float)
        self._pfi_arr_raw = np.zeros(self.all_time, dtype=float)
        self._deriv_arr = np.zeros(self.all_time, dtype=float)
        self._w_arr = np.zeros(self.all_time, dtype=float)
        self._delta_pfi_raw = self.delta_pfi
        self._delta_pfi_old = 0
        self.control = 0
        
        
    def draw_content(self):

        zoom = 15

        pl.plot(np.linspace(0,1,self._pfi_arr.shape[0]), zoom*self._pfi_arr)
        pl.plot(np.linspace(0,1,self._pfi_arr_raw.shape[0]), zoom*self._pfi_arr_raw + 2)

        pl.axis('off')
        
        self.axes.set_xlim([0, 1])
        self.axes.set_ylim([-1, 3])
            

    def update_state(self, batch, t):
    
        # Making random walk signal with limitations; this version will get control on it
        z = np.random.rand()
        
        delta_pfi = self.delta_pfi + 0.001*np.sign(z-0.5)
        
        if delta_pfi > 0.05:
            delta_pfi = 0.05
        
        if delta_pfi < -0.05:
            delta_pfi = -0.05
            
        self.delta_pfi = delta_pfi

        # Making random walk signal with limitations; this is just to look what would have happened if there were no control
        
        delta_pfi_raw = self._delta_pfi_raw + 0.001*np.sign(z-0.5)
        if delta_pfi_raw > 0.05:
            delta_pfi_raw = 0.05

        if delta_pfi_raw < -0.05:
            delta_pfi_raw = -0.05
            
        self._delta_pfi_raw = delta_pfi_raw
      
        self.delta_pfi -= self.control*self.delta_pfi # adding control here

        # calculating derivative
        if t>1:
            deriv = self.delta_pfi - self._delta_pfi_old
            self._deriv_arr[:-1] = self._deriv_arr[1:]
            self._deriv_arr[-1] = deriv

        # Saving values of the signal 

        self._pfi_arr[:-1] = self._pfi_arr[1:]
        self._pfi_arr[-1] = delta_pfi

        # Control rate, its essentially the learned weights w. But was checking with constant, too.
        
        self._pfi_arr_raw[:-1] = self._pfi_arr_raw[1:]
        self._pfi_arr_raw[-1] = self._delta_pfi_raw  # Here I am collecting random walk signal in case it was not controlled, not very orderly, should have done higher

        self._delta_pfi_old = self.delta_pfi
        

    def move(self, control):
        self.control = control


