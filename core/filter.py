import numpy as np
import pylab as pl


class FilterBank(object):
    def __init__(self, f, Q, hist_len=100):
        """
        Filter bank consisting of resonators with frequency f and quality Q.
        hist_len determines the length the impule response. See [1] for
        details.
        
        References:
        
        [1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
        convergence of temporal sequence learning by using input correlations only."
        Neural computation 18.6 (2006): 1380-1412.
        
        https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
        """
        self.f = f
        self.Q = Q
        self.hist_len = hist_len if self.Q is not None else 1
        self.num_filters = f.shape[0]
        self.reset()
        
                
    def reset(self):
        """
        Resets the filter bank and computes the impulse response
        kernel h, to be used in update method.
        """
        if self.Q is None:
            # no filter on input
            num_filters = self.f.shape[0]
            self.h = np.ones([num_filters,1])
        else:
            a = -np.pi*self.f/self.Q
            b = np.sqrt((2*np.pi*self.f)**2 - a**2)
            num_filters = a.shape[0]
            hist_len = self.hist_len

            n = np.tile(np.arange(hist_len),[num_filters,1]) 
            a = np.tile(a,[hist_len,1]).T
            b = np.tile(b,[hist_len,1]).T
            
            # This implements Eq.(4) of [1].        
            self.h = 1/b * np.exp(a*n) * np.sin(b*n)
        
        self.history = np.zeros_like(self.h)
        self.state = np.zeros(num_filters)


    def update(self, x):
        """
        Apply filter bank to input x. Returns tuple containing current
        state and state increments.
        """
        self.history = np.hstack([x.reshape([-1,1]), self.history[:,:-1]])
        new_state = (self.history*self.h).sum(1)
        d_state = new_state - self.state
        self.state = new_state
        return self.state, d_state


