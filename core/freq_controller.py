import numpy as np
import pylab as pl

from .environment import Environment

class Arena(Environment):
    """
    Arena implements the closed loop robot environment from Figure 3 in [1].
    
    References:
    
    [1] Porr, Bernd, and Florentin Wörgötter. "Strongly improved stability and faster
    convergence of temporal sequence learning by using input correlations only."
    Neural computation 18.6 (2006): 1380-1412.
    
    https://www.berndporr.me.uk/ico_neco/porr_woe_neco_ico09final2c.pdf
    """
    def __init__(self, frequency=1, phase=0, target_freq=1, target_phase=0, visible=True):
        super().__init__(visible, plot_interval=10)
        self.frequency = frequency
        self.phase = phase
        self.target_freq = target_freq
        self.target_phase = target_phase
        self.reset()
        

    def reset(self):
        self._cur_time_0 = .0
        self._cur_time_1 = .0
        self._traj_0 = np.zeros(500)
        self._traj_1 = np.zeros(500)
        
        
    def draw_content(self):

        pl.plot(np.linspace(0,1,self._traj_0.shape[0]), self._traj_0)
        pl.plot(np.linspace(0,1,self._traj_0.shape[0]), self._traj_1 + 2)

        pl.text(0.2,0,"freq="+str(self.target_freq))
        pl.text(0.2,2,"freq="+str(self.frequency))

        pl.axis('off')
        
        self.axes.set_xlim([0, 1])
        self.axes.set_ylim([-1, 3])
            

    def update_state(self, batch, t):
    
        self._cur_time_0 += self.target_freq/(2*np.pi)
        self._cur_time_1 += self.frequency/(2*np.pi)

        self._traj_0[:-1] = self._traj_0[1:]
        self._traj_1[:-1] = self._traj_1[1:]
        
        self._traj_0[-1] = np.sin(self._cur_time_0 + self.target_phase)
        self._traj_1[-1] = np.sin(self._cur_time_1 + self.phase)
        

    def move(self, frequency):
        self.frequency = frequency


