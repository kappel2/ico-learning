#!/usr/bin/env python3

import numpy as np
import pylab as pl

from ico_noise_cancellation import ico_noise_cancellation


if __name__=="__main__":

    cutoff_all = np.linspace(8000,11000,15)
    
    snr_times_all = []
    snrs_all = []

    for i in range(len(cutoff_all)):
        print("run #"+str(i))
        results = ico_noise_cancellation(T_max=5, cutoff=cutoff_all[i], plot=False)
        
        snr_times_all += [results["snr_times"]]
        snrs_all += [results["snr_values"]]
        
        print("mean noise reduction: "+str(snrs_all[-1][-10:].mean()))

    pl.errorbar( cutoff_all, np.array( snrs_all )[:,15:20].mean(1), \
                 yerr=np.array( snrs_all )[:,15:20].std(1) )
                 
    pl.xlabel("cutoff frequency of low pass filter")
    pl.ylabel("noise reduction [dB]")
    
    pl.show()
    import ipdb; ipdb.set_trace()

